﻿using OpenQA.Selenium;

namespace BBCTests.Pages
{
    public class SignInPopup 
    {
        protected readonly IWebElement element;
        private IWebElement MaybeLaterButton => element.FindElement(By.XPath(".//button[@class='sign_in-exit']"));

        public SignInPopup(IWebElement element)
        {
            this.element = element;
        }

        public void CloseSignInPopupIfNeeded() {
            if(MaybeLaterButton.Displayed)
                MaybeLaterButton.Click();
        }
    }
}