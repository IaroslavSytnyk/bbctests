﻿Feature: Submit story to BBC

Scenario Outline: Check error massege while creating article
	Given I go to 'News' tab on the main page    
	And I go to 'Coronavirus' news tab
	And I go to Your Coronavirus Stories tab on coronavirus page
	And I go to 'How to share' on your coronavirus stories page 
	When I submit '<credentials>' credentials with '<article text>' article text 
	Then I see an '<error messages>' error message

	Examples: 
	| credentials                               | article text | error messages                                                       |
	| Valid credentials                         |              | can't be blank                                                       |
	| Credentials without name                  | test         | Name can't be blank                                                  |
	| Credentials without acceptance age        | test         | must be accepted                                                     |
	| Credentials without acceptance user terms | test         | must be accepted                                                     |
	| Credentials without required fields       |              | can't be blank,Name can't be blank,must be accepted,must be accepted |