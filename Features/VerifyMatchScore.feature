﻿Feature: Verify match score

Background: 
	Given I go to 'Sport' tab on the main page
	And I go to 'Football' sport tab
	And I go to 'Scores & Fixtures' on football page
	And I search championship by 'Premier League'
	And I select 'SEP' month results

Scenario: Check Score On Scores And Fixtures Page
	Then I see match between 'Leicester City' and 'Newcastle United' teams with '5-0' score

Scenario: Check Score On Match Page
	When I go to 'Leicester City - Newcastle United' match page
	Then I see '5-0' score for this match