﻿using System.Collections.Generic;

namespace BBCTests.StepsDefinition.Data
{
    public static class CredentialManager
    {
        public static Dictionary<CredentialKey, Credentials> Credentials => new Dictionary<CredentialKey, Credentials>(){
            {CredentialKey.ValidCredentials, new Credentials(){
            UserName = "Test",
            Email = "test@gmail.com",
            ContactNumber = "+380440000001",
            Location = "Ukraine",
            IsIncognito = true,
            IsOver16 = true,
            IsTermsAccepted = true} },

            {CredentialKey.CredentialsWithoutName, new Credentials(){
            UserName = "",
            Email = "test@gmail.com",
            ContactNumber = "+380440000001",
            Location = "Ukraine",
            IsIncognito = true,
            IsOver16 = true,
            IsTermsAccepted = true} },

             {CredentialKey.CredentialsWithoutAcceptanceAge, new Credentials(){
            UserName = "Test",
            Email = "test@gmail.com",
            ContactNumber = "+380440000001",
            Location = "Ukraine",
            IsIncognito = true,
            IsOver16 = false,
            IsTermsAccepted = true} },

            {CredentialKey.CredentialsWithoutAcceptanceUserTerms, new Credentials(){
            UserName = "Test",
            Email = "test@gmail.com",
            ContactNumber = "+380440000001",
            Location = "Ukraine",
            IsIncognito = true,
            IsOver16 = true,
            IsTermsAccepted = false} },

            {CredentialKey.CredentialsWithoutRequiredFields, new Credentials(){
            UserName = "",
            Email = "",
            ContactNumber = "",
            Location = "",
            IsIncognito = false,
            IsOver16 = false,
            IsTermsAccepted = false} },
        };
    }
}