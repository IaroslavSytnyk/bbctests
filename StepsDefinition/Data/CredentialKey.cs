﻿namespace BBCTests.StepsDefinition.Data
{
   public enum CredentialKey
    {
        ValidCredentials,
        CredentialsWithoutName,
        CredentialsWithoutAcceptanceAge,
        CredentialsWithoutAcceptanceUserTerms,
        CredentialsWithoutRequiredFields
    }
}
