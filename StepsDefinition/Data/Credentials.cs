﻿namespace BBCTests.StepsDefinition.Data
{
   public class Credentials
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Location { get; set; }
        public bool IsIncognito { get; set; }
        public bool IsOver16 { get; set; }
        public bool IsTermsAccepted { get; set; }
    }
}