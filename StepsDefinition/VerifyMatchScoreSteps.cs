﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace BBCTests.StepsDefinition
{
    [Binding]
    public class VerifyMatchScoreSteps : BaseSteps
    {
        [Given(@"I go to '(.*)' sport tab")]
        public void GivenIGoToSportTab(string tabName) => sportPage.GoToSportTab(tabName);

        [Given(@"I go to '(.*)' on football page")]
        public void GivenIGoToOnFootballPage(string tabname) => footballPage.GoToFootballTab(tabname);

        [Given(@"I search championship by '(.*)'")]
        public void GivenISearchChampionshipBy(string championship) => scoresAndFixturesPage.SetSearchingtext(championship);

        [Given(@"I select '(.*)' month results")]
        public void GivenISelectMonthResults(string month) => sportSearchResultsPage.SelectMonth(month);

        [When(@"I go to '(.*)' match page")]
        public void WhenIGoToMatchPage(string match)
        {
            var teams = match.Split(" - ");
            sportSearchResultsPage.GoToMatch(teams.First(), teams.Last());
        }

        [Then(@"I see match between '(.*)' and '(.*)' teams with '(.*)' score")]
        public void ThenISeeMatchBetweenAndTeamsWithScore(string firstTeam, string secondTeam, string score)
        {
            var expectedScore = score.Split("-");
            var actualScore = sportSearchResultsPage.GetMatchScoreForTeams(firstTeam, secondTeam);
            Assert.AreEqual((expectedScore.First(), expectedScore.Last()), actualScore, "Actual score is not equal to expected.");
        }

        [Then(@"I see '(.*)' score for this match")]
        public void ThenISeeScoreForThisMatch(string score)
        {
            var expectedScore = score.Split("-");
            Assert.AreEqual((expectedScore.First(), expectedScore.Last()), (matchPage.GetFirstTeamScore(), matchPage.GetSecondTeamScore()), "Actual score is not equal to expected.");
        }
    }
}