﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCTests.Pages
{
    public class FootballPage : BasePage
    {
        public FootballPage(IWebDriver driver) : base(driver) { }

        private IEnumerable<IWebElement> FootballTabs => driver.FindElements(By.XPath("//ul[@aria-label='Secondary']//li"));

        public void GoToFootballTab(string tabName)
        {
            FootballTabs.First(s => s.Text.Equals(tabName)).Click();
            WaitForPageLoadComplete();
        }
    }
}