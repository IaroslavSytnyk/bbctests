﻿using OpenQA.Selenium;

namespace BBCTests.Pages
{
    public class MatchPage : BasePage
    {
        public MatchPage(IWebDriver driver) : base(driver) { }

        private IWebElement FirstTeamScore => driver.FindElement(By.XPath(".//span[contains(@class,'number--home')]"));

        private IWebElement SecondTeamScore => driver.FindElement(By.XPath(".//span[contains(@class,'number--away')]"));

        public string GetFirstTeamScore() => FirstTeamScore.Text;

        public string GetSecondTeamScore() => SecondTeamScore.Text;
    }
}