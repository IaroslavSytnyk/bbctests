﻿using BBCTests.StepsDefinition.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCTests.Pages
{
    public class HowToSharePage : BasePage
    {
        public HowToSharePage(IWebDriver driver) : base(driver) { }

        private IWebElement TellUsYourStoryTextField => driver.FindElement(By.XPath("//textarea"));

        private IWebElement NameTextField => driver.FindElement(By.XPath("//input[@aria-label='Name']"));

        private IWebElement EmailAddressTextField => driver.FindElement(By.XPath("//input[@aria-label='Email address']"));
        
        private IWebElement ContactNumberTextField => driver.FindElement(By.XPath("//input[@aria-label='Contact number ']"));
        
        private IWebElement LocationTextField => driver.FindElement(By.XPath("//input[@aria-label='Location ']"));

        private IWebElement GetCheckboxWithTitle(string checkboxName) =>
            driver.FindElement(By.XPath($"//input[@type='checkbox' and following-sibling::span[div/p[contains(text(),\"{checkboxName}\")]]]"));

        private IWebElement SubmitButton => driver.FindElement(By.XPath("//button[contains(text(),'Submit')]"));

        private IEnumerable<IWebElement> ErrorMessagesList => driver.FindElements(By.XPath("//div[@class='input-error-message']"));

        public void SetHowToShareArticleData(Credentials credentials, string articleText)
        {
            TellUsYourStoryTextField.SendKeys(articleText);
            NameTextField.SendKeys(credentials.UserName);
            EmailAddressTextField.SendKeys(credentials.Email);
            ContactNumberTextField.SendKeys(credentials.ContactNumber);
            LocationTextField.SendKeys(credentials.Location);
        }

        public void SetArticleCheckboxes(Credentials credentials)
        {
            if (credentials.IsIncognito)
                GetCheckboxWithTitle("Please don't publish my name").Click();
            if (credentials.IsOver16)
                GetCheckboxWithTitle("I am over 16 years old").Click();
            if (credentials.IsTermsAccepted)
                GetCheckboxWithTitle("I accept the ").Click();
        }

        public void SubmitArticleData() 
        {
            SubmitButton.Click();
            WebDriverWait waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            waiter.Until(y => !SubmitButton.GetAttribute("class").Contains("inactive"));
        }
        public void SubmitCredentialsAndArticleText(Credentials credentials, string articleText)
        {
            SetHowToShareArticleData(credentials, articleText);
            SetArticleCheckboxes(credentials);
            SubmitArticleData();
        }

        public List<string> GetErrorMessages() => ErrorMessagesList.Select(s => s.Text).ToList();
    }
}