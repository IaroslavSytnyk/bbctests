﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCTests.Pages
{
    public class SportPage : BasePage
    {
        public SportPage(IWebDriver driver) : base(driver) { }

        private IEnumerable<IWebElement> SportTabs => driver.FindElements(By.XPath("//ul[contains(@class,'sport-navigation')]//li"));

        public void GoToSportTab(string tabName)
        {
            SportTabs.First(s => s.Text.Equals(tabName)).Click();
            WaitForPageLoadComplete();
        }
    }
}