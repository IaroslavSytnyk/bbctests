﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace BBCTests.Pages
{
    public class YourCoronavirusStoriesPage : BasePage
    {
        public YourCoronavirusStoriesPage(IWebDriver driver) : base(driver) { }

        private IEnumerable<IWebElement> GetInTouchTabs => driver.FindElements(By.XPath("//div[contains(@aria-labelledby,'Getintouchwithus')]//a"));
        
        public void GoToGetInTouchTab(string tabName)
        {
            GetInTouchTabs.First(s => s.Text.Contains(tabName)).Click();
            WaitForPageLoadComplete();
        }
    }
}