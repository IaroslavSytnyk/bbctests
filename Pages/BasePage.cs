﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCTests.Pages
{
    public abstract class BasePage
    {
        protected readonly IWebDriver driver;
        protected readonly long timeout = 3000;

        protected BasePage(IWebDriver driver)
        {
            this.driver = driver;
            ImplicitWait();
        }

        private IWebElement SearchTextField => driver.FindElement(By.XPath("//input[@id='orb-search-q']"));

        private IEnumerable<IWebElement> NavigationTabsList => driver.FindElements(By.XPath("//nav[@role='navigation']//li"));

        private SignInPopup signInPopup => new SignInPopup(driver.FindElement(By.XPath("//div[@id='sign_in']")));

        public void ImplicitWait() => driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(timeout);

        public void WaitForPageLoadComplete()
        {
            try
            {
                IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout));
                wait.Until(driver1 => ((IJavaScriptExecutor)driver)
                .ExecuteScript("return document.readyState").Equals("complete"));
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
        }

        public void ScrollToElement(IWebElement element)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }
        
        public void SetSearchText(string value)
        {
            SearchTextField.SendKeys(value);
            SearchTextField.SendKeys(Keys.Return);
            WaitForPageLoadComplete();
        }

        public void GoToNavigationTab(string tabName)
        {
            NavigationTabsList.First(n => n.Text.Equals(tabName)).Click();
            WaitForPageLoadComplete();
        }
        public void CloseSignInPopupIfNeeded()
        {
            signInPopup.CloseSignInPopupIfNeeded();
        }
    }
}